# WWplot

A simple plotting tool for experimental physics classes.

![](wwplot_adapta_theme.png) 


In order to run this program you will need:

- Python 3 with Numpy, Scipy, PyGobject 3, cairocffi, Matplotlib
- Gtk 3

After installing the required libraries execute the main.py script in a
terminal:

	python main.py

This program is developed and tested on Linux. I am not sure if it runs on Windows

This project has been moved to https://github.com/wwmm/wwplot. From now on only github repository will be updated